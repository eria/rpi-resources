# ejercicio 3 - vilanet2013

import RPi.GPIO as GPIO
import sys, time, signal

from LCD import * # Importamos la libreria que se encarga de manejar el display LCD

def signal_handler(signal, frame):
    GPIO.cleanup()
    print('[*] Exiting...')
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

lcd = LCD() # Inicializamos el objeto de la clase LCD

while True: # Bucle que imprime caracteres en pantalla
  # La posición de los argumenos de show hacen referencia a la posicion de las lines del display LCD
  lcd.show("abcdefghijklmnop","   qrstuvwxyz   ")  
  time.sleep(5)
  lcd.show("ABCDEFGHIJKLMNOP","   QRSTUVWXYZ   ")
  time.sleep(5)
  lcd.show("   1234567890   ",".:;()=[]-+*¿?!¿¡")
  time.sleep(2)
  # Se puede mostrar texto solo en una linea
  lcd.show(line1="    >linea 1   ")
  lcd.show(line2="    >linea 2   ")

