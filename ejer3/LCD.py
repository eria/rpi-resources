import RPi.GPIO as GPIO
import time

# Define GPIO to LCD mapping
LCD_RS = 8
LCD_E  = 24
LCD_D4 = 23 
LCD_D5 = 18
LCD_D6 = 15
LCD_D7 = 14

# Define some device constants
LCD_WIDTH = 16    # Maximum characters per line
LCD_CHR = True
LCD_CMD = False

LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line 

# Timing constants
E_PULSE = 0.00005
E_DELAY = 0.00005

class LCD:
    blocked=False
    def __init__(self):
        GPIO.setmode(GPIO.BCM)       # Use BCM GPIO numbers
        GPIO.setup(LCD_E, GPIO.OUT)  # E
        GPIO.setup(LCD_RS, GPIO.OUT) # RS
        GPIO.setup(LCD_D4, GPIO.OUT) # DB4
        GPIO.setup(LCD_D5, GPIO.OUT) # DB5
        GPIO.setup(LCD_D6, GPIO.OUT) # DB6
        GPIO.setup(LCD_D7, GPIO.OUT) # DB7


    def reset(self):
        # Initialise display
        self.lcd_byte(0x33,LCD_CMD)
        self.lcd_byte(0x32,LCD_CMD)
        self.lcd_byte(0x28,LCD_CMD)
        self.lcd_byte(0x0C,LCD_CMD)  
        self.lcd_byte(0x06,LCD_CMD)
        self.lcd_byte(0x01,LCD_CMD) 
        time.sleep(0.001)

    def show(self, line1=None, line2=None):
        while self.blocked: continue
        self.blocked=True

        self.reset()

        if(line1 != None):
            self.lcd_byte(LCD_LINE_1, LCD_CMD)
            mLine1 = line1.ljust(LCD_WIDTH, " ")
            for i in range(LCD_WIDTH):
                self.lcd_byte(ord(mLine1[i]),LCD_CHR)
        if(line2 != None):
            self.lcd_byte(LCD_LINE_2, LCD_CMD)
            mLine2 = line2.ljust(LCD_WIDTH, " ")
            for i in range(LCD_WIDTH):
                self.lcd_byte(ord(mLine2[i]),LCD_CHR)
        self.blocked=False


    def lcd_byte(self, bits, mode):
        # Send byte to data pins
        # bits = data
        # mode = True  for character
        #        False for command

        GPIO.output(LCD_RS, mode) # RS

        # High bits
        GPIO.output(LCD_D4, False)
        GPIO.output(LCD_D5, False)
        GPIO.output(LCD_D6, False)
        GPIO.output(LCD_D7, False)
        if bits&0x10==0x10:
            GPIO.output(LCD_D4, True)
        if bits&0x20==0x20:
            GPIO.output(LCD_D5, True)
        if bits&0x40==0x40:
            GPIO.output(LCD_D6, True)
        if bits&0x80==0x80:
            GPIO.output(LCD_D7, True)

        # Toggle 'Enable' pin
        time.sleep(E_DELAY)    
        GPIO.output(LCD_E, True)  
        time.sleep(E_PULSE)
        GPIO.output(LCD_E, False)  
        time.sleep(E_DELAY)      

        # Low bits
        GPIO.output(LCD_D4, False)
        GPIO.output(LCD_D5, False)
        GPIO.output(LCD_D6, False)
        GPIO.output(LCD_D7, False)
        if bits&0x01==0x01:
            GPIO.output(LCD_D4, True)
        if bits&0x02==0x02:
            GPIO.output(LCD_D5, True)
        if bits&0x04==0x04:
            GPIO.output(LCD_D6, True)
        if bits&0x08==0x08:
            GPIO.output(LCD_D7, True)

        # Toggle 'Enable' pin
        time.sleep(E_DELAY)    
        GPIO.output(LCD_E, True)  
        time.sleep(E_PULSE)
        GPIO.output(LCD_E, False)  
        time.sleep(E_DELAY)

    def clear(self):
        # Initialise display
        self.lcd_byte(0x33,LCD_CMD)
        self.lcd_byte(0x32,LCD_CMD)
        self.lcd_byte(0x28,LCD_CMD)
        self.lcd_byte(0x0C,LCD_CMD)  
        self.lcd_byte(0x06,LCD_CMD)
