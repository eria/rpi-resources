# ejercicio 1 - vilanet2013

import RPi.GPIO as GPIO # Importamos la libreria para manejo de GPIOs
import time
import signal
import sys

PIN = 17

def signal_handler(signal, frame): # Funcion de salida
    GPIO.output(17, False) # Desactivamos la salida 17 del GPIO
    GPIO.cleanup() # Regeneramos el mapeado del GPIO
    print('[*] Saliendo...')
    sys.exit(0) 

GPIO.setmode(GPIO.BCM) # Usamos la nomenglatura BCM para los pines de la RPi

signal.signal(signal.SIGINT, signal_handler)

GPIO.setup(PIN, GPIO.OUT) # Configuramos el Pin 17  del GPIO como una salida
while True:
    GPIO.output(PIN, GPIO.HIGH) # Activamos la salida 17 del GPIO
    time.sleep(1) # Esperamos durante un sengundo
    GPIO.output(PIN, GPIO.LOW) # Desactivamos la salida 17 del GPIO
    time.sleep(1) # Esperamos durante un segundo
