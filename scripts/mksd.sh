#!/bin/bash
# Script para la creacion de la tarjeta SD

#  Primero preparar una SD con el FS
#  y todo lo que necesites. Luego:
#   mkdir /tmp/release
#   mount /dev/sdb2 /tmp/release/
#   mount /dev/sdb1 /tmp/release/boot/
#	tar czpvf /tmp/release.tgz --directory=/tmp/release/ .
#	umount /tmp/release/boot 
#	umount /tmp/release/

if [ "$#" -lt "3" ]
then
    echo "Usage: "
    echo " $0 </dev/sdX> <dest dir> <ip>"
    exit
fi

RELEASE="/tmp/release.tgz"
DEVICE=$1
TMP_DIR=$2
IP=$3

umount $DEVICE"1"
umount $DEVICE"2"

echo "Formatting SD…"
#Formateo la SD y creo la particion de boot y raiz
dd if=/dev/zero of=$DEVICE bs=512 count=1 conv=notrunc
parted -s $DEVICE mklabel msdos 2>&1
parted -s $DEVICE mkpart primary fat32 4096s 60M 2>&1
parted -s $DEVICE mkpart primary ext4 60M 100% 2>&1
mkfs.vfat -F 32 $DEVICE"1"
mkfs.ext4 -F -I 256 -E stride=2,stripe-width=1024,nodiscard -b 4096 $DEVICE"2"
tune2fs -c 0 $DEVICE"2"

echo "Mounting..."
#Copio en la particion del ext4 el fs
mkdir $TMP_DIR
mount $DEVICE"2" $TMP_DIR/

#Copio en la particion del fat el boot
mkdir $TMP_DIR/boot
mount $DEVICE"1" $TMP_DIR/boot

echo "Extracting files..."
#Extraigo el fs del paquete
tar xzpf $RELEASE --directory=$TMP_DIR

echo "Configuring..."
#provisional
echo "auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
       address $IP
       netmask 255.255.255.0
       gateway 192.168.1.1
       dns-nameservers 8.8.8.8" > $TMP_DIR/etc/network/interfaces
       
echo "Syncing..."
#Borro los directorios auxiliares
sync

echo "Umounting..."
umount $DEVICE"1"
umount $DEVICE"2"

rmdir $TMP_DIR

echo "DONE $IP"
