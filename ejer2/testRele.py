# ejercicio 2 - vilanet2013

import RPi.GPIO as GPIO
import sys, time, signal

def activate(pin):
    print("Activating relay: %s" % pin)
    GPIO.output(pin, False)

def timed(pin, secs):
    activate(pin)
    time.sleep(secs)
    deactivate(pin)

def deactivate(pin):
    print("Closing relay: %s" % pin)
    GPIO.output(pin, True)

def signal_handler(signal, frame): # Funcion de salida
    GPIO.cleanup() # Regeneramos el mapeado del GPIO
    print('[*] Saliendo...')
    sys.exit(0) 

relay1  = 23 
relay2  = 24

GPIO.setmode(GPIO.BCM) # Usamos la nomenglatura BCM para los pines de la RPi

GPIO.setup(relay1, GPIO.OUT, initial=GPIO.HIGH) # Configuramos el Pin 17  del GPIO como una salida
GPIO.setup(relay2, GPIO.OUT, initial=GPIO.HIGH) # Configuramos el Pin 17  del GPIO como una salida

signal.signal(signal.SIGINT, signal_handler)

timed(relay1, 3)

while True:
    activate(relay2)
    time.sleep(2)
    deactivate(relay2)
    time.sleep(2)

